#!/bin/vbash
# shellcheck shell=bash

# Returns the Nth IP address in the provided prefix
nth_in_prefix() { # $1 - prefix, $2 - number of address
	if [[ -z "${1:-}" ]] || [[ -z "${2:-}" ]]; then
		echo 'IP prefix or the number of the address not provided, aborting...'
		builtin exit 1
	fi

	local a b c d
	IFS='.' read -r a b c d <<< "$1"
	if [[ "${a}" -ge 0 ]] && [[ "${a}" -le 255 ]] && [[ "${b}" -ge 0 ]] && [[ "${b}" -le 255 ]] && [[ "${c}" -ge 0 ]] && [[ "${c}" -le 255 ]] && [[ "${d}" -ge 0 ]] && [[ "${d}" -le 255 ]]; then
		printf '%s' "${a}.${b}.${c}.$((d+$2))"
	else
		echo 'Invalid IP prefix provided, aborting...'
		builtin exit 1
	fi
}

# Returns the WAN IP, retrying if not available
get_wan_ip() {
	local wan_ip

	while true; do
		# shellcheck disable=SC2312 # We only care about the final output
		wan_ip="$(run show dhcp client leases interface eth1 | grep -a 'IP address' | sed -n -E 's/[^0-9]*([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})[^0-9]*/\1/p')"
		[[ "${wan_ip}" != '' ]] && break
		echo 'Unable to get WAN IP, waiting 10 seconds...'
		sleep 10
	done

	printf '%s' "${wan_ip}"
}

# Returns the DHCP server subnet IP prefix
get_dhcp_server_prefix() {
	# Extract the network from VyOS config
	local prefix

	# shellcheck disable=SC2312 # We only care about the exit code
	if ! prefix="$(run show configuration json | jq -r '.service."dhcp-server"."shared-network-name"."darillium-pool".subnet | keys[0]')"; then
		echo 'Unable to get DHCP server subnet IP prefix, aborting...'
		builtin exit 1
	fi

	# Extract the prefix from the CIDR notation
	if ! grep --quiet --text '/' <<< "${prefix}"; then
		echo 'The returned subnet is not using CIDR notation, aborting...'
		builtin exit 1
	fi

	prefix="$(cut --delimiter '/' --fields 1 <<< "${prefix}")"

	printf '%s' "${prefix}"
}

# Returns the DHCP server subnet mask
get_dhcp_server_mask() {
	# Extract the network from VyOS config
	local mask

	# shellcheck disable=SC2312 # We only care about the exit code
	if ! mask="$(run show configuration json | jq -r '.service."dhcp-server"."shared-network-name"."darillium-pool".ssubnet | keys[0]')"; then
		echo 'Unable to get DHCP server subnet mask, aborting...'
		builtin exit 1
	fi

	# Extract the mask from the CIDR notation
	if ! grep --quiet --text '/' <<< "${mask}"; then
		echo 'The returned subnet is not using CIDR notation, aborting...'
		builtin exit 1
	fi

	mask="$(cut --delimiter '/' --fields 2 <<< "${mask}")"

	printf '%s' "${mask}"
}

# Prepare BIND config from the provided template
write_bind_config() { # $1 - template path
	export LAN_NETWORK_PREFIX
	export LAN_NETWORK_MASK

	# shellcheck disable=SC2016 # We want to control which environment variables envsubst replaces
	new_config_content="$(envsubst '$LAN_NETWORK_PREFIX,$LAN_NETWORK_MASK' < "$1")"
	printf '%s' "${new_config_content}" | sudo tee '/config/user-data/bind-config/named.conf' > '/dev/null'
}

# Prepare the external zone file from the provided template
write_internal_zone_file() { # $1 - template path
	ROUTER_IP="$(nth_in_prefix "${LAN_NETWORK_PREFIX:?}" 1)"
	SERVER_IP="$(nth_in_prefix "${LAN_NETWORK_PREFIX:?}" 3)"
	export ROUTER_IP
	export SERVER_IP

	# shellcheck disable=SC2016 # We want to control which environment variables envsubst replaces
	new_zone_content="$(envsubst '$ROUTER_IP,$SERVER_IP' < "$1")"
	printf '%s' "${new_zone_content}" | sudo tee "${1/.zone.in/.internal.zone}" > '/dev/null'
}

# Prepare the external zone file from the provided template
write_external_zone_file() { # $1 - template path
	ROUTER_IP="$(get_wan_ip)"
	SERVER_IP="${ROUTER_IP}"
	export ROUTER_IP
	export SERVER_IP

	# shellcheck disable=SC2016 # We want to control which environment variables envsubst replaces
	new_zone_content="$(envsubst '$ROUTER_IP,$SERVER_IP' < "$1")"
	printf '%s' "${new_zone_content}" | sudo tee "${1/.zone.in/.external.zone}" > '/dev/null'
}
