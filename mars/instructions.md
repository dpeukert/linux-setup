1. Configure RouterOS via serial
   1. Accept ToS
   2. Set RouterOS admin password
   3. Change identity - `/system/identity/set name=mars`
   4. Delete default IP adress - `/ip/address/remove 0`
   5. Add DHCP client - `/ip/dhcp-client/add interface=bridge disabled=no`
2. Upgrade everything via RouterOS WebFig
   1. Upgrade RouterOS - `/system/package/update/install`
   2. Upgrade SwOS - `/system/swos/upgrade`
   3. Upgrade RouterBOOT - `/system/routerboard/upgrade`
3. Reboot - `/system/reboot`
4. Configure boot options
   1. Press `Delete` while rebooting
   2. Press `d` and set `1s` as the boot delay
   3. Press `j` and set `SwOS` as the boot OS
5. Boot into SwOS
6. Configure SwOS via web
   1. Link/ETH/BOOT/Name - `ETHER1`
   2. System/Address Acquisition - `DHCP only`
   3. System/Identity - `mars`
   4. System/Mikrotik Discovery Protocol - disable for all ports
   5. System/Old password - empty
   6. System/New password and Confirm password - generate password (maximum length is 15 characters)
   7. Upgrade/Download & Upgrade to get rid of `A backup version of SwOS is running, update SwOS for full functionality.`
