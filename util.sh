#!/bin/bash

# User options
# shellcheck disable=SC2034 # This variable used by scripts importing this file
timezone='Europe/Prague'

# Constants
# shellcheck disable=SC2034 # This variable used by scripts importing this file
newline='
'

# Helpers
abort () {
	if [[ -n "${2:-}" ]]; then echo "$2"; fi
	builtin exit "${1:-1}"
}

is_true () {
	if [[ "$#" -ne 1 ]]; then
		abort 1 'is_true: exactly one parameter must be provided'
	fi

	[[ "$1" = '1' ]] || [[ "$1" = 'true' ]] || [[ "$1" = 'yes' ]] && return 0 || return 1
}

is_false () {
	if [[ "$#" -ne 1 ]]; then
		abort 1 'is_false: exactly one parameter must be provided'
	fi

	[[ "$1" = '0' ]] || [[ "$1" = 'false' ]] || [[ "$1" = 'no' ]] && return 0 || return 1
}

# String manipulation
trimstring () {
	if [[ "$#" -ne 1 ]]; then
		abort 1 'trimstring: exactly one parameter must be provided'
	fi

	printf '%s' "$1" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'
}

stringtobytes () {
	if [[ "$#" -ne 1 ]]; then
		abort 1 'stringtobytes: exactly one parameter must be provided'
	fi

	string="$(trimstring "$1")"
	string="$(hexdump --no-squeezing --format '"\\" 1/1 "%.2x"' <<< "${string}")"
	tr '[:lower:]' '[:upper:]' <<< "${string}"
}

# Validation
isvalidprompttype () {
	if [[ "$#" -ne 1 ]]; then
		abort 1 'isvalidprompttype: exactly one parameter must be provided'
	fi

	if [[ -z "$1" ]]; then return 1; fi
	[[ "$1" = 'string' ]] || [[ "$1" = 'bytestring' ]] || [[ "$1" = 'bool' ]] || [[ "$1" = 'ip' ]] || [[ "$1" = 'ipprefix' ]] || [[ "$1" = 'mac' ]] && return 0 || return 1
}

isvalidip () { # https://www.linuxjournal.com/content/validating-ip-address-bash-script
	if [[ "$#" -ne 1 ]]; then
		abort 1 'isvalidip: exactly one parameter must be provided'
	fi

	local ip_to_validate="$1"
	local ip_sections
	local stat='1'

	if [[ "${ip_to_validate}" =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
		mapfile -t -d '.' ip_sections <<< "${ip_to_validate}"

		[[ "${ip_sections[0]}" -le 255 && "${ip_sections[1]}" -le 255 && "${ip_sections[2]}" -le 255 && "${ip_sections[3]}" -le 255 ]]
		stat="$?"
	fi

	return "${stat}"
}

isvalidmac () {
	if [[ "$#" -ne 1 ]]; then
		abort 1 'isvalidmac: exactly one parameter must be provided'
	fi

	[[ "$1" =~ ^([0-9A-Fa-f]{2}[:-]){5}[0-9A-Fa-f]{2}$ ]] && return 0 || return 1
}

# Input handling
inputselect () {
	if [[ "$#" -lt 2 ]]; then
		abort 1 'inputselect: two or more parameters must be provided'
	fi

	unset VALUE
	PS3="$1: "
	shift 1

	select VALUE in "$@"; do
		if [[ -n "${VALUE}" ]]; then
			break
		fi
	done
}

inputprompt () {
	if [[ "$#" -ne 2 ]] && [[ "$#" -ne 3 ]]; then
		abort 1 'inputprompt: two or three parameters must be provided'
	fi

	optional_param="${3:-false}"
	[[ "${optional_param}" = 'true' ]] && optional_prompt=' (optional)' || optional_prompt=''

	unset VALUE

	case "$1" in
	'string')
		read -r -p "$2${optional_prompt}: " VALUE
	;;
	'bytestring')
		read -r -p "$2${optional_prompt}: " VALUE
		VALUE="$(stringtobytes "${VALUE}")"
	;;
	'bool')
		read -r -N 1 -p "$2 [Y/n] " VALUE

		if [[ "${VALUE}" != $'\n' ]]; then printf '\n'; fi

		if [[ ${VALUE} =~ ^[Yy] ]] || [[ "${VALUE}" = $'\n' ]]; then
			VALUE='true'
		elif [[ ${VALUE} =~ ^[Nn] ]]; then
			VALUE='false'
		else
			VALUE=''
		fi
	;;
	'ip')
		read -r -p "$2 (e.g. 198.51.100.25)${optional_prompt}: " VALUE
		VALUE="$(trimstring "${VALUE}")"

		if ! isvalidip "${VALUE}"; then VALUE=''; fi
	;;
	'ipprefix')
		read -r -p "$2 (e.g. 198.51.100.0/24)${optional_prompt}: " VALUE

		IFS='/' read -ra PREFIXPARTS <<< "${VALUE}"

		if [[ "${#PREFIXPARTS[@]}" -eq 2 ]]; then
			ip="$(trimstring "${PREFIXPARTS[0]}")"
			mask="$(trimstring "${PREFIXPARTS[1]}")"

			if isvalidip "${ip}" && [[ "${mask}" -ge 1 ]] && [[ "${mask}" -le 32 ]]; then
				VALUE="${ip}/${mask}"
			else
				VALUE=''
			fi
		else
			VALUE=''
		fi
	;;
	'mac')
		read -r -p "$2 (e.g. 90:10:2B:AF:A3:8A)${optional_prompt}: " VALUE
		VALUE="$(trimstring "${VALUE}")"
		VALUE="$(tr '[:lower:]' '[:upper:]' <<< "${VALUE}")"
		VALUE="$(tr '-' ':' <<< "${VALUE}")"

		if ! isvalidmac "${VALUE}"; then VALUE=''; fi
	;;
	*)
		VALUE=''
	;;
	esac

	if [[ "${VALUE}" = '' ]]; then
		if [[ "${optional_param}" = 'false' ]] || [[ "$1" = 'bool' ]]; then
			inputprompt "$1" "$2" "${optional_param}"
		fi
	fi
}

# Config handling
loadconfig() {
	if [[ "$#" -ne 2 ]]; then
		abort 1 'loadconfig: exactly two parameters must be provided'
	fi

	if [[ ! -f "$2" ]]; then
		abort 1 "Config file $2 doesn't exist, aborting..."
	fi

	while read -r line; do
		if printf '%s' "${line}" | grep -qv '^#'; then
			eval "$1_${line}"
		fi
	done < "$2"
}

# Input/output manipulation
startredirect () {
	# Store the original FDs 1 and 2 in FDs 3 and 4 and set FDs 1 and 2 to our log file
	# shellcheck disable=SC2154 # ${dir} is specified in the script sourcing this file
	exec 3>&1 4>&2 1>> "${dir}/isidore.log" 2>> "${dir}/isidore.log"
}

stopredirect () {
	# Set FDs 1 and 2 to their original destinations stored in FDs 3 and 4
	exec 1>&3 2>&4
}

log () {
	if [[ "$#" != '1' ]]; then
		abort 1 'log: exactly one parameter must be provided'
	fi

	printf '%s' "$1"
	startredirect
	echo "--- $1 ---"
}

checkmark () {
	stopredirect
	printf ' \033[0;32m✓\033[0m\n'
}
