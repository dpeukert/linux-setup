server {
	include /etc/nginx/listen.conf;
	server_name salvador.dpk.es;

	include /etc/nginx/no-ai.conf;

	location / {
		proxy_pass http://salvador:80;
	}

	include /etc/nginx/securitytxt.conf;
}
