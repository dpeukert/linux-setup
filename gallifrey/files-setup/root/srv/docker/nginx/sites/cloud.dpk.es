server {
	include /etc/nginx/listen.conf;
	server_name cloud.dpk.es;

	include /etc/nginx/no-ai.conf;

	location / {
		proxy_pass http://nextcloud:8000;
	}

	location ^~ /.well-known {
		return 301 https://$host/index.php$request_uri;
	}

	location = /.well-known/carddav {
		return 301 https://$host/remote.php/dav;
	}

	location = /.well-known/caldav {
		return 301 https://$host/remote.php/dav;
	}

	location = /.well-known/change-password {
		return 302 https://$host/settings/user/security;
	}

	location = /remote {
		return 301 https://$host/remote.php;
	}

	location /remote/ {
		rewrite ^/remote/(.*)$ /remote.php/$1 permanent;
	}

	include /etc/nginx/securitytxt.conf;
}
