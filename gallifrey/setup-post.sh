#!/bin/bash

log 'SET UP SECONDARY URLWATCH'
	cat "/home/$device/private/isidore_files/$device/urlwatch/config-secondary" | sudo tee '/srv/urlwatch/config-secondary' > '/dev/null'
	cat "/home/$device/private/isidore_files/$device/urlwatch/urls-secondary" | sudo tee '/srv/urlwatch/urls-secondary.yaml' > '/dev/null'
	sudo chown "$device:users" '/srv/urlwatch/config-secondary' '/srv/urlwatch/urls-secondary.yaml'
	chmod 600 '/srv/urlwatch/config-secondary' '/srv/urlwatch/urls-secondary.yaml'
checkmark

log 'CONFIGURING SERVICES'
	sudo systemctl enable --now 'urlwatch-secondary.timer'
checkmark
