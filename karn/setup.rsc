-----BEGIN CONFIG-----
# variable name;variable type;prompt;optional;conditional prompt variable;conditional prompt value
password;bytestring;Choose your web interface password;false;false
enable2ghz;bool;Do you want to enable the 2.4GHz antennae?;false;false
2ghzSSID;bytestring;Choose your 2.4GHz SSID;enable2ghz;true
2ghzPSK;bytestring;Choose your 2.4GHz PSK;enable2ghz;true
enable5ghz;bool;Do you want to enable the 5GHz antennae?;false;false
5ghzSSID;bytestring;Choose your 5GHz SSID;enable5ghz;true
5ghzPSK;bytestring;Choose your 5GHz PSK;enable5ghz;true
-----END CONFIG-----

# Setup logging
:local errorMsg "";
:local restoreToDefaults false;
:local logfile [/file get [find name~"/output.rsc\$"] name];

/system logging action add name=setup target=disk disk-file-count=1 disk-file-name=($logfile) disk-lines-per-file=65535;
/system logging add action=setup;

:do {
:log info "### SETUP SCRIPT STARTED ###";

:log info "--- Validating config ---";
:if ([:typeof $hostname] != "str" or $hostname = "") do={
	:set errorMsg "Invalid config: \$hostname must be a non-empty string";
	:error $errorMsg;
}

:if ([:typeof $password] != "str" or $password = "") do={
	:set errorMsg "Invalid config: \$password must be a non-empty string";
	:error $errorMsg;
}

:if ([:typeof $enable2ghz] != "bool") do={
	:set errorMsg "Invalid config: \$enable2ghz must be a boolean";
	:error $errorMsg;
}

:if ($enable2ghz = true) do={
	:if ([:typeof $2ghzSSID] != "str" or $2ghzSSID = "") do={
		:set errorMsg "Invalid config: \$2ghzSSID must be a non-empty string";
		:error $errorMsg;
	}

	:if ([:typeof $2ghzPSK] != "str" or $2ghzPSK = "") do={
		:set errorMsg "Invalid config: \$2ghzPSK must be a non-empty string";
		:error $errorMsg;
	}
}

:if ([:typeof $enable5ghz] != "bool") do={
	:set errorMsg "Invalid config: \$enable5ghz must be a boolean";
	:error $errorMsg;
}

:if ($enable5ghz = true) do={
	:if ([:typeof $5ghzSSID] != "str" or $5ghzSSID = "") do={
		:set errorMsg "Invalid config: \$5ghzSSID must be a non-empty string";
		:error $errorMsg;
	}

	:if ([:typeof $5ghzPSK] != "str" or $5ghzPSK = "") do={
		:set errorMsg "Invalid config: \$5ghzPSK must be a non-empty string";
		:error $errorMsg;
	}
}

:log info "--- Setting up a new account and removing default admin account ---";
:if ([/user find where name=($hostname)] = "") do={
	/user add name=($hostname) password=($password) group=full;
}
/user remove admin;

:log info "--- Setting miscellaneous options ---";
/system identity set name=($hostname);
/system script add name=dark-mode source={
	:if ([/system leds settings get all-leds-off] = "never") do={
		/system leds settings set all-leds-off=immediate
	} else={
		/system leds settings set all-leds-off=never
	}
}
/system routerboard mode-button set enabled=yes on-event=dark-mode;
/system routerboard settings set silent-boot=yes;
/system ntp client set enabled=yes;
/system ntp client servers add address=0.pool.ntp.org;
/system ntp client servers add address=1.pool.ntp.org;
/system ntp client servers add address=2.pool.ntp.org;
/system ntp client servers add address=3.pool.ntp.org;
/ip ssh set strong-crypto=yes;
/ip ssh set host-key-type=ed25519;
/ip ssh regenerate-host-key;
/ip settings set rp-filter=strict;

:log info "--- Configuring and disabling services ---";
/ip service set ssh port=2222;
/user ssh-keys import user=($hostname) public-key-file=[/file get [find name~"/ssh.pub\$"] name]
/ip service set www port=8080;
/ip service disable telnet,ftp,www-ssl,api,winbox,api-ssl;
/tool mac-server set allowed-interface-list=none;
/tool mac-server mac-winbox set allowed-interface-list=none;
/tool mac-server ping set enabled=no;
/tool bandwidth-server set enabled=no;
/ip neighbor discovery-settings set discover-interface-list=none;

:log info "--- Initializing ethernet interfaces ---";
:local count 0;
:while ([/interface ethernet find] = "") do={
	:if ($count = 30) do={
		:set errorMsg "Unable to find ethernet interfaces after 30 seconds";
		:error $errorMsg;
	}
	:delay 1s;
	:set count ($count + 1);
}

:log info "--- Initializing wireless interfaces ---";
:set count 0;
:if ($enable2ghz = true) do={
	:while ([/interface wireless find where band~"^2ghz"] = "") do={
		:if ($count = 30) do={
			:set errorMsg "Unable to find 2.4GHz wireless interfaces after 30 seconds";
			:error $errorMsg;
		}
		:delay 1s;
		:set count ($count + 1);
	}
}

:set count 0;
:if ($enable5ghz = true) do={
	:while ([/interface wireless find where band~"^5ghz"] = "") do={
		:if ($count = 30) do={
			:set errorMsg "Unable to find wireless interfaces after 30 seconds";
			:error $errorMsg;
		}
		:delay 1s;
		:set count ($count + 1);
	}
}

:log info "--- Setting up wireless interfaces ---";
:if ($enable2ghz = true) do={
	/interface wireless security-profiles add name=($hostname . "-security-2ghz") mode=dynamic-keys authentication-types=wpa2-psk disable-pmkid=yes unicast-ciphers=aes-ccm group-ciphers=aes-ccm wpa2-pre-shared-key=($2ghzPSK)
	/interface wireless set [find where band~"^2ghz"] name=($hostname . "-wireless-2ghz") wireless-protocol=802.11 mode=ap-bridge band=2ghz-g/n channel-width=20mhz scan-list=2412,2437,2462 frequency-mode=regulatory-domain country="czech republic" antenna-gain=2 installation=indoor distance=indoors wps-mode=disabled ssid=($2ghzSSID) security-profile=($hostname . "-security-2ghz") disabled=no;
}

:if ($enable5ghz = true) do={
	/interface wireless security-profiles add name=($hostname . "-security-5ghz") mode=dynamic-keys authentication-types=wpa2-psk disable-pmkid=yes unicast-ciphers=aes-ccm group-ciphers=aes-ccm wpa2-pre-shared-key=($5ghzPSK)
	/interface wireless set [find where band~"^5ghz"] name=($hostname . "-wireless-5ghz") wireless-protocol=802.11 mode=ap-bridge band=5ghz-a/n/ac channel-width=20/40/80mhz-Ceee frequency=auto frequency-mode=regulatory-domain country="czech republic" antenna-gain=3 installation=indoor distance=indoors wps-mode=disabled ssid=($5ghzSSID) security-profile=($hostname . "-security-5ghz") disabled=no;
}

:log info "--- Setting up bridge ---";
/interface bridge add name=($hostname . "-bridge") disabled=no auto-mac=yes protocol-mode=none;

:local bridgeMACSet 0;
:foreach tmpPort in=[/interface find where !(slave=yes or name=($hostname . "-bridge"))] do={
	:local tmpPortName [/interface get $tmpPort name];
	:if ($bridgeMACSet = 0 and [/interface get $tmpPort type] = "ether") do={
		:log info "Using $tmpPortName's MAC address for $hostname-bridge";
		/interface bridge set ($hostname . "-bridge") auto-mac=no admin-mac=[/interface ethernet get $tmpPort mac-address];
		:set bridgeMACSet 1;
	}
	:log info "Adding $tmpPortName to $hostname-bridge";
	/interface bridge port add bridge=($hostname . "-bridge") interface=($tmpPortName);
}

/ip dhcp-client add interface=($hostname . "-bridge") disabled=no;

:log info "### SETUP SCRIPT FINISHED ###";
} on-error {
	:set restoreToDefaults true;
	:if ($errorMsg != "") do={
		:log error $errorMsg;
	}
	:log error "Error handler called, resetting to default configuration";
}

/system logging remove [/system logging find where action=setup];
/system logging action remove setup;

:if ($restoreToDefaults = true) do={
	/system reset-configuration no-defaults=no skip-backup=yes;
}
