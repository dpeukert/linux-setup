server {
	include /etc/nginx/listen.conf;
	server_name mta-sts.peukert.cc;

	include /etc/nginx/no-ai.conf;

	location / {
		return 404;
	}

	include /etc/nginx/mta-sts.conf;
	include /etc/nginx/securitytxt.conf;
	include /etc/nginx/robotstxt-disallow.conf;
}
