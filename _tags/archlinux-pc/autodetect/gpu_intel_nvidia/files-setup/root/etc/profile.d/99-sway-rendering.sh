#!/bin/sh

# Set sway rendering options if relevant
if [ -f '/etc/is_pc' ]; then
    export WLR_DRM_DEVICES='/dev/dri/by-name/intel:/dev/dri/by-name/nvidia'
fi
