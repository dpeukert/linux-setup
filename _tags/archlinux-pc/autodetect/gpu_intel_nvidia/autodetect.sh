#!/bin/bash

pci_devices="$(lspci -nn)"
gpus="$(grep '\[03' <<< "${pci_devices}")"
gpus="$(cut -d ':' -f 3 <<< "${gpus}")"
gpu_count="$(wc -l <<< "${gpus}")"

if [[ "${gpu_count}" = '2'  ]] && grep -q '[[:space:]]*Intel Corporation' <<< "${gpus}" && grep -q '[[:space:]]*NVIDIA Corporation' <<< "${gpus}"; then
	exit 0
fi

exit 1
