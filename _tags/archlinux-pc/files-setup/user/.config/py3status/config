general {
	interval = 1
	color = '#F1F1F1'
	color_good = '#F1F1F1'
	color_bad = '#F1F1F1'
	color_degraded = '#F1F1F1'
}

py3status {
	separator_block_width = 15
}

imap {
	cache_timeout = 120
	on_click 1 = 'exec open-or-focus thunderbird'
	user = 'daniel@peukert.cc'
	password = shell(cat ~/.config/py3status/imap-pwd, str)
	server = shell(cat ~/.config/py3status/imap-server, str)
	port = 993
	security = 'ssl'
	hide_if_zero = true
	markup = 'pango'
	format = '󰇰<span letter_spacing="-6000"> </span>{unseen}'
}

arch_updates {
	cache_timeout = 120
	on_click 1 = 'exec miniterm -t "Updates" -e "dpm update"'
	hide_if_zero = true
	markup = 'pango'
	format = '󰏗<span letter_spacing="-6000"> </span>{pacman}/{aur}'
}

playerctl {
	cache_timeout = 5
	button_previous = 3
	button_play_pause = 2
	button_next = 1
	button_stop = None
	players = ['tidal-hifi']
	format_player = '[\?color=status [{artist}] - [{title:}]]'
	color_bad = '#F1F1F180'
	thresholds = {"status": [("Playing", "good"), ("Paused", "bad"), ("Stopped", "bad")]}
	sanitize_words = ['bonus', 'demo', 'edit', 'explicit', 'extended', 'feat', 'mono', 'remaster', 'stereo', 'version', 'live', 'original mix']
}

volume_status {
	cache_timeout = 5
	on_click 1 = 'exec pavucontrol'
	button_mute = 2
	button_down = 4
	button_up = 5
	command = 'pactl'
	max_volume = 100
	volume_delta = 5
	blocks = '󰕿󰕿󰖀󰕾'
	markup = 'pango'
	format = '{icon}<span letter_spacing="-6000"> </span>{percentage}'
	format_muted = '󰖁<span letter_spacing="-6000"> </span>{percentage}'
	color_bad = '#FF0000'
	thresholds = [
		(0, '#F1F1F180'),
		(1, '#F1F1F1')
	]
}

wifi {
	cache_timeout = 5
	on_click 1 = 'exec miniterm -t "WiFi" -e "iwd-menu"'
	blocks = '󰤯󰤟󰤢󰤥󰤨'
	markup = 'pango'
	format = '[{icon}<span letter_spacing="-4000"> </span>{ssid}]|󰤮'
}

bluetooth {
	cache_timeout = 5
	on_click 1 = 'exec miniterm -t "Bluetooth" -e "bluetuith"'
	markup = 'pango'
	format_adapter = '\?if=powered [\?if=!format_device 󰂯|󰂱<span letter_spacing="-4000"> </span>{format_device}]|󰂲'
	format_adapter_separator = ', '
	format_device = '\?if=connected {name}<span letter_spacing="-4000"> </span>(<span letter_spacing="-12000"> </span>󰁹<span letter_spacing="-9000"> </span>{battery})'
	format_device_separator = ', '
}

external_script {
	cache_timeout = 10
	on_click 1 = 'exec dssh toggle'
	on_click 3 = 'exec dssh toggle'
	script_path = 'dssh status'
	convert_numbers = false
	format = '\?if=output [\?color=#F1F1F180&show 󰷖]|󰷖'
}

systemd {
	cache_timeout = 10
	on_click 1 = 'exec systemctl is-active openvpn-client@work.service && miniterm -t "VPN" -e "sudo systemctl stop openvpn-client@work.service" || miniterm -t "VPN" -e "sudo systemctl start openvpn-client@work.service"'
	on_click 3 = 'exec systemctl is-active openvpn-client@work.service && miniterm -t "VPN" -e "sudo systemctl stop openvpn-client@work.service" || miniterm -t "VPN" -e "sudo systemctl start openvpn-client@work.service"'
	unit = 'openvpn-client@work.service'
	markup = 'pango'
	format = '󰒄[\?if=status=active |[\?if=status=inactive |<span letter_spacing="-4000"> </span>{status}]]'
	color_bad = '#F1F1F180'
	color_degraded = '#F0A000'
}

battery_level {
	cache_timeout = 120
	blocks = '󰂎󰁺󰁻󰁼󰁽󰁾󰁿󰂀󰂁󰂂󰁹'
	charging_character = '󰂄'
	markup = 'pango'
	format = '{icon}<span letter_spacing="-9000"> </span>[\?if=percent>100 100|{percent}]'
	threshold_bad = 15
	color_charging = '#00FF00'
	color_bad = '#FF0000'
}

time {
	on_click 1 = 'exec miniterm -t "Calendar" -e "dcal"'
	format = '%Y/%m/%d %H:%M:%S'
}
