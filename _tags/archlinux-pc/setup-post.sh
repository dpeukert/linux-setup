#!/bin/bash

log 'SETTING REGDOMAIN'
	sudo iw reg set "$country"
	sudo sed 's/^#WIRELESS_REGDOM="CZ"$/WIRELESS_REGDOM="CZ"/' -i '/etc/conf.d/wireless-regdom'
checkmark

log 'COPYING AND MODIFYING PC FILES'
	# SSH
	cat "/home/$device/private/isidore_files/archlinux-pc/ssh/config" >> "/home/$device/.ssh/config"

	cat "/home/$device/private/isidore_files/archlinux-pc/ssh/id_ed25519" > "/home/$device/.ssh/id_ed25519"
	chmod 644 "/home/$device/.ssh/id_ed25519.pub"
	chmod 600 "/home/$device/.ssh/id_ed25519"

	# Fonts
	sudo cp -r --no-preserve=mode "/home/$device/private/isidore_files/archlinux-pc/fonts/" '/usr/share/fonts/private/'
	sudo chown -R 'root:root' '/usr/share/fonts/private/'
	sudo fc-cache -fv

	# File manager bookmarks
	cat "/home/$device/private/isidore_files/archlinux-pc/bookmarks" > "/home/$device/.config/gtk-3.0/bookmarks"

	# Code config
	for code_settings_file in "/home/$device/.config/Code/User/settings."*'.json'; do
		jq -s '.[0] * .[1]' "/home/$device/.config/Code/User/settings.json" "$code_settings_file" > "/home/$device/.config/Code/User/settings.merged.json"
		rm "/home/$device/.config/Code/User/settings.json" "$code_settings_file"
		mv "/home/$device/.config/Code/User/settings.merged.json" "/home/$device/.config/Code/User/settings.json"
	done

	# IMAP
	cat "/home/$device/private/isidore_files/archlinux-pc/imap-pwd" > "/home/$device/.config/py3status/imap-pwd"
	cat "/home/$device/private/isidore_files/archlinux-pc/imap-server" > "/home/$device/.config/py3status/imap-server"
	chmod 600 "/home/$device/.config/py3status/imap-pwd" "/home/$device/.config/py3status/imap-server"

	# makepkg config
	sudo sed "s/#PACMAN_AUTH.*$/PACMAN_AUTH=('bash' '-c' '--' '%c')/" -i '/etc/makepkg.conf'
	echo "PACMAN='trizen'" | sudo tee -a '/etc/makepkg.conf' > '/dev/null'

	# dbackup
	mkdir -p "/home/$device/.config/dbackup/"
	cat "/home/$device/private/isidore_files/archlinux-pc/dbackup-config" > "/home/$device/.config/dbackup/config"
	chmod 600 "/home/$device/.config/dbackup/config"

	# py3status
	if [ -f "/home/$device/.config/py3status/order" ]; then
		echo >> "/home/$device/.config/py3status/config"
		cat "/home/$device/.config/py3status/order" >> "/home/$device/.config/py3status/config"
		rm "/home/$device/.config/py3status/order"
	fi
checkmark

log 'MODIFYING PHP CONFIG FILES'
	php_config_changes=(
		'memory_limit=-1'
		'post_max_size=0'
		'upload_max_filesize=0'
		'extension=bcmath'
		'extension=ftp'
		'extension=gd'
		'extension=iconv'
		'extension=intl'
		'extension=mysqli'
		'extension=pdo_mysql'
		'extension=pdo_pgsql'
		'extension=sockets'
		'zend_extension=opcache'
	)

	for prop in "${php_config_changes[@]}"; do
		prop_name="$(printf '%s' "$prop" | cut -f1 -d '=')"
		prop_value="$(printf '%s' "$prop" | cut -f2 -d '=')"

		if [ "$prop_name" = 'extension' ] || [ "$prop_name" = 'zend_extension' ]; then
			sudo sed "s/^;*$prop_name\s*=\s*$prop_value.*$/$prop_name=$prop_value/" -i '/etc/php/php.ini'
		else
			sudo sed "s/^;*$prop_name\s*=.*$/$prop_name=$prop_value/" -i '/etc/php/php.ini'
		fi
	done

	echo -e 'zend_extension=xdebug.so\nxdebug.mode=off\n;xdebug.mode=debug\n;xdebug.start_with_request=yes\n;xdebug.max_nesting_level=500\n;xdebug.overload_var_dump=off' | sudo tee '/etc/php/conf.d/xdebug.ini' > '/dev/null'
checkmark

log 'CONFIGURING GIT'
	git config --global user.signingKey '~/.ssh/id_ed25519'
	git config --global gpg.format 'ssh'
	git config --global commit.gpgsign true
	git config --global tag.gpgsign true
	git config --global push.gpgsign 'if-asked'
checkmark

log 'IMPORTING MONGODB COMPASS CONNECTIONS'
	# Temporarily install xvfb, as mongodb-compass does not install without a display server
	echo 'xorg-server-xvfb' > '/tmp/xvfb.list'
	installpackages '/tmp/xvfb.list'
	rm -f '/tmp/xvfb.list'

	# Import the connections
	cat "/home/$device/private/isidore_files/archlinux-pc/compass-connections" > '/tmp/compass-connections'
	xvfb-run mongodb-compass-isolated-beta --import-connections='/tmp/compass-connections'
	rm -f '/tmp/compass-connections'

	# Uninstall xvfb
	uninstallpackages 'xorg-server-xvfb'
checkmark

log 'INSTALLING CODE PACKAGES'
	if [ -f "$dir/_lists/code.list" ]; then
		cat "$dir/_lists/code.list" | while read -r ext; do
			code --force --install-extension "$ext"
		done
	fi
checkmark

log 'APPLYING PC SETTINGS'
	# dconf Editor
	gsettings set 'ca.desrt.dconf-editor.Settings'						'show-warning'						false

	# Drawing
	gsettings set 'com.github.maoschanz.drawing'						'big-icons'							true
	gsettings set 'com.github.maoschanz.drawing'						'dark-theme-variant'				true
	gsettings set 'com.github.maoschanz.drawing'						'disabled-tools' 					"['skew', 'points']"
	gsettings set 'com.github.maoschanz.drawing'						'replace-alpha'						'white'
	gsettings set 'com.github.maoschanz.drawing'						'show-labels'						true

	# File Roller
	gsettings set 'org.gnome.FileRoller.Dialogs.New'					'default-extension'					'.zip'

	# GTK
	gsettings set 'org.gnome.desktop.interface'							'color-scheme'						'prefer-dark'
	gsettings set 'org.gnome.desktop.interface'							'cursor-theme'						'Adwaita'
	gsettings set 'org.gnome.desktop.interface'							'font-name'							'Roboto 10'
	gsettings set 'org.gnome.desktop.interface'							'gtk-theme'							'Adwaita'
	gsettings set 'org.gnome.desktop.interface'							'icon-theme'						'Adwaita'
	gsettings set 'org.gnome.desktop.interface'							'monospace-font-name'				'DejaVu Sans Mono 10'
	gsettings set 'org.gtk.Settings.FileChooser'						'sort-directories-first'			true
	gsettings set 'org.gtk.Settings.FileChooser'						'startup-mode'						'cwd'

	# Nemo
	gsettings set 'org.cinnamon.desktop.default-applications.terminal'	'exec'								'miniterm'
	gsettings set 'org.cinnamon.desktop.media-handling'					'automount-open'					false
	gsettings set 'org.cinnamon.desktop.media-handling'					'automount'							false
	gsettings set 'org.cinnamon.desktop.media-handling'					'autorun-never'						true
	gsettings set 'org.nemo.desktop'									'show-desktop-icons'				false
	gsettings set 'org.nemo.list-view'									'default-column-order'				"['name', 'size', 'mime_type', 'date_modified']"
	gsettings set 'org.nemo.list-view'									'default-visible-columns'			"['name', 'size', 'mime_type', 'date_modified']"
	gsettings set 'org.nemo.plugins'									'disabled-actions'					"['send-by-mail.nemo_action', 'add-desklets.nemo_action', 'set-as-background.nemo_action', 'new-launcher.nemo_action', 'change-background.nemo_action', 'set-resolution.nemo_action', '90_new-launcher.nemo_action']"
	gsettings set 'org.nemo.preferences.menu-config'					'selection-menu-pin'				false
	gsettings set 'org.nemo.preferences'								'close-device-view-on-device-eject'	false
	gsettings set 'org.nemo.preferences'								'date-format'						'informal'
	gsettings set 'org.nemo.preferences'								'default-folder-viewer'				'list-view'
	gsettings set 'org.nemo.preferences'								'detect-content'					false
	gsettings set 'org.nemo.preferences'								'disable-menu-warning'				true
	gsettings set 'org.nemo.preferences'								'ignore-view-metadata'				true
	gsettings set 'org.nemo.preferences'								'show-advanced-permissions'			true
	gsettings set 'org.nemo.preferences'								'show-compact-view-icon-toolbar'	false
	gsettings set 'org.nemo.preferences'								'show-directory-item-counts'		'always'
	gsettings set 'org.nemo.preferences'								'show-edit-icon-toolbar'			false
	gsettings set 'org.nemo.preferences'								'show-full-path-titles'				true
	gsettings set 'org.nemo.preferences'								'show-icon-view-icon-toolbar'		false
	gsettings set 'org.nemo.preferences'								'show-image-thumbnails'				'never'
	gsettings set 'org.nemo.preferences'								'show-list-view-icon-toolbar'		false
	gsettings set 'org.nemo.preferences'								'show-location-entry'				true
	gsettings set 'org.nemo.preferences'								'size-prefixes'						'base-2'
	gsettings set 'org.nemo.window-state'								'start-with-menu-bar'				false

	# File assignments
	xdg-mime default 'code-oss.desktop'						'application/javascript'
	xdg-mime default 'code-oss.desktop'						'application/json'
	xdg-mime default 'code-oss.desktop'						'application/octet-stream'
	xdg-mime default 'code-oss.desktop'						'application/x-desktop'
	xdg-mime default 'code-oss.desktop'						'application/x-shellscript'
	xdg-mime default 'code-oss.desktop'						'application/xhtml+xml'
	xdg-mime default 'code-oss.desktop'						'application/xml'
	xdg-mime default 'code-oss.desktop'						'text/css'
	xdg-mime default 'code-oss.desktop'						'text/csv'
	xdg-mime default 'code-oss.desktop'						'text/html'
	xdg-mime default 'code-oss.desktop'						'text/markdown'
	xdg-mime default 'code-oss.desktop'						'text/plain'
	xdg-mime default 'code-oss.desktop'						'text/x-python'
	xdg-mime default 'code-oss.desktop'						'text/xml'
	xdg-mime default 'firefox-developer-edition.desktop'	'application/pdf'
	xdg-mime default 'firefox-developer-edition.desktop'	'x-scheme-handler/about'
	xdg-mime default 'firefox-developer-edition.desktop'	'x-scheme-handler/http'
	xdg-mime default 'firefox-developer-edition.desktop'	'x-scheme-handler/https'
	xdg-mime default 'firefox-developer-edition.desktop'	'x-scheme-handler/unknown'
	xdg-mime default 'imv.desktop'							'image/bmp'
	xdg-mime default 'imv.desktop'							'image/gif'
	xdg-mime default 'imv.desktop'							'image/jpeg'
	xdg-mime default 'imv.desktop'							'image/png'
	xdg-mime default 'imv.desktop'							'image/svg+xml'
	xdg-mime default 'imv.desktop'							'image/tiff'
	xdg-mime default 'imv.desktop'							'image/x-png'
	xdg-mime default 'mpv.desktop'							'application/x-extension-mp4'
	xdg-mime default 'mpv.desktop'							'video/avi'
	xdg-mime default 'mpv.desktop'							'video/mkv'
	xdg-mime default 'mpv.desktop'							'video/mp4'
	xdg-mime default 'mpv.desktop'							'video/webm'
	xdg-mime default 'mpv.desktop'							'video/x-matroska'
	xdg-mime default 'nemo.desktop'							'inode/directory'
	xdg-mime default 'thunderbird.desktop'					'x-scheme-handler/mailto'
checkmark

log 'ADDING GROUPS'
	sudo usermod -aG audio "$device"
	sudo usermod -aG realtime "$device"
checkmark
