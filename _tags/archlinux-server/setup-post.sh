#!/bin/bash

log 'COPYING SERVER FILES'
	# Environment file for Docker Compose
	if [ -f "/home/$device/private/isidore_files/$device/.env" ]; then
		cat "/home/$device/private/isidore_files/$device/.env" | sudo tee '/srv/docker/.env' > '/dev/null'
		sudo chmod 600 '/srv/docker/.env'
	fi

	# Docker config for the root-ran Docker Compose
	if [ -f "/home/$device/private/isidore_files/$device/docker-config" ]; then
		sudo mkdir --parents '/root/.docker/'
		sudo chmod 700 '/root/.docker/'

		cat "/home/$device/private/isidore_files/$device/docker-config" | sudo tee '/root/.docker/config.json' > '/dev/null'
		sudo chmod 600 '/root/.docker/config.json'
	fi
checkmark

log 'SET UP URLWATCH'
	# Copy urlwatch config and URLs
	sudo mkdir --parents '/srv/urlwatch/'
	cat "/home/$device/private/isidore_files/$device/urlwatch/config" | sudo tee '/srv/urlwatch/config' > '/dev/null'
	cat "/home/$device/private/isidore_files/$device/urlwatch/urls" | sudo tee '/srv/urlwatch/urls.yaml' > '/dev/null'
	sudo chown "$device:users" '/srv/urlwatch/config' '/srv/urlwatch/urls.yaml'
	chmod 600 '/srv/urlwatch/config' '/srv/urlwatch/urls.yaml'

	# Set device name in urlwatch service
	sudo sed -i "s/%DEVICE%/$device/g" '/etc/systemd/system/urlwatch.service'
checkmark

log 'CONFIGURING SERVER SERVICES'
	sudo /usr/local/bin/letsencrypt-renew --force
	sudo systemctl enable --now 'docker-compose.service'
	sudo systemctl enable --now 'maintenance.timer'
	sudo systemctl enable --now 'urlwatch.timer'
checkmark
