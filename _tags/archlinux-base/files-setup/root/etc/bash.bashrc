#!/bin/bash

# If not interactive, stop here
[[ $- != *i* ]] && return

# Setup shell prompt
get_user_prompt() {
	local current_user
	local current_hostname

	current_user="$(whoami)"
	current_hostname="$(hostname)"

	if [[ "${current_user}" = "${current_hostname}" ]]; then
		printf '%s' "${current_hostname}"
	else
		printf '%s@%s' "${current_user}" "${current_hostname}"
	fi
}

get_git_prompt() {
	if git rev-parse --is-inside-work-tree > '/dev/null' 2>&1; then
		local remote_url
		local repo_name
		local repo_status

		remote_url="$(git remote get-url origin)"
		repo_name="$(basename -s .git "${remote_url}")"
		repo_status="$(git branch | sed -n '/\* /s///p')"
		printf ' %s#%s ' "${repo_name}" "${repo_status}"
	fi
}

export PS1='\[$(tput setaf 0)\]\[$(tput setab 2)\] $(get_user_prompt) \[$(tput setab 4)\]$(get_git_prompt)\[$(tput setaf 0)\]\[$(tput setab 7)\] \w \[$(tput sgr0)\] '
export PROMPT_COMMAND='history -a; history -n; printf "\033]0;%s:%s\007" "$(get_user_prompt)" "${PWD/#$HOME/\~}"'

# Configure shell options
shopt -s autocd
shopt -s cdspell
shopt -s histappend

# Set aliases
alias sudo='sudo '

alias cat='ccat'
alias cp='cp -v'
alias diff='colordiff'
alias grep='grep --color=auto'
alias http='curlie'
alias ls='ls --group-directories-first --color=auto'
alias mv='mv -v'
alias rm='rm -v'
alias watch='watch --color'

if [[ -r '/usr/bin/go-task' ]]; then
	alias task='go-task'
fi

alias c='clear'
alias l='ls -1'
alias ll='ls -lah'
alias tailf='tail -f'
alias x='exit'

alias i='dpm install'
alias r='dpm remove'
alias u='dpm update'

alias sp='sudo systemctl suspend'
alias sd='sudo systemctl poweroff'
alias rb='sudo systemctl reboot'

if pgrep --uid "${UID}" --exact sway > '/dev/null'; then
	alias lk='loginctl lock-session'
fi

# Setup bash completion
if [[ -r '/usr/share/bash-completion/bash_completion' ]]; then
	. '/usr/share/bash-completion/bash_completion'
fi

# Setup pkgfile for not found commands
if [[ -r '/usr/share/doc/pkgfile/command-not-found.bash' ]]; then
	. '/usr/share/doc/pkgfile/command-not-found.bash'
fi

# Setup bash completion for aliases
if [[ -r '/usr/share/bash-complete-alias/complete_alias' ]]; then
	# shellcheck source=/dev/null
	. '/usr/share/bash-complete-alias/complete_alias'
	complete -F _complete_alias "${!BASH_ALIASES[@]}"
fi

# Setup replacement bash completion for git clone
if [[ -r '/usr/share/gcac/gcac.bash' ]]; then
	. '/usr/share/gcac/gcac.bash'
fi
