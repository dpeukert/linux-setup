#!/bin/bash
# dependencies: docker-compose (optional) fwupd pacman pacman-contrib trizen

# Set shell options
set -e -u -o 'pipefail'
shopt -s 'nullglob'

# Define our helper functions
abort() { # $1 - exit code, $2 - message
	echo "$2"
	exit "$1"
}

remove_orphans() { # $@ - packages to ignore
	# Get orphans and convert them to an array
	local orphans
	orphans="$(trizen --query --unrequired --deps --quiet || true)"
	mapfile -t orphans <<< "${orphans}"

	# Ignore empty strings in orphan array
	for orphan_package_index in "${!orphans[@]}"; do
		if [[ -z "${orphans[orphan_package_index]}" ]]; then
			unset 'orphans[orphan_package_index]'
		fi
	done

	# Ignore packages passed as parameters
	for package_to_ignore in "$@"; do
		for orphan_package_index in "${!orphans[@]}"; do
			if [[ "${orphans[orphan_package_index]}" = "${package_to_ignore}" ]]; then
				unset 'orphans[orphan_package_index]'
			fi
		done
	done

	# If we have any orphans left, remove them
	if [[ "${#orphans[@]}" -gt 0 ]]; then
		trizen --remove --nosave --recursive "${orphans[@]}"
	fi
}

get_paccache_package_count() { # $1 - paccache output
	if echo "$1" | grep 'no candidate packages' > '/dev/null'; then
		echo '0'
	else
		echo "$1" | sed --quiet --regexp-extended 's/.* ([0-9]+) packages removed.*$/\1/p'
	fi
}

get_paccache_package_size() { # $1 - paccache output
	if echo "$1" | grep 'no candidate packages' > '/dev/null'; then
		echo '0'
	else
		echo "$1" | sed --quiet --regexp-extended --expression 's/.*\(disk space saved: (.*)B\)$/\1/' --expression 's/ //p' | numfmt --from=iec-i
	fi
}

# Act based on the provided command
case "${1:-}" in
'install')
	shift 1
	trizen --sync --asdeps "$@"
	remove_orphans "$@"
;;
'remove')
	shift 1
	trizen --remove --nosave --recursive "$@"
	remove_orphans
;;
'update')
	# Update Arch packages
	trizen --sync --refresh --sysupgrade --regular

	# Update AUR packages
	trizen --sync --refresh --sysupgrade --aur --devel --needed

	# Remove orphans
	remove_orphans

	# Update firmware
	printf '\n\033[1;34m::\033[0m\033[1m Refreshing firmware metadata...\033[0m\n'
	fwupdmgr refresh || true

	printf '\033[1;34m::\033[0m\033[1m Checking for firmware updates...\033[0m\n'
	update='0'

	# Get devices with an available update
	devices="$(fwupdmgr get-updates --json | jq --raw-output '.Devices[] | (.Name // .DeviceId + " (" + .Plugin + ")")')"

	# Check if we got any devices back
	if [[ -n "${devices}" ]]; then
		mapfile -t devices <<< "${devices}"
		device_count="${#devices[@]}"

		if [[ "${device_count}" -gt 0 ]]; then
			# We did, build a string to present a list to the user
			first_device_printed='0'

			prompt='Updates for devices '
			for device in "${devices[@]}"; do
				if [[ "${first_device_printed}" != '0' ]]; then
					prompt+=', '
				else
					first_device_printed='1'
				fi

				prompt+="${device}"
			done
			prompt+=' are available, do you want to install them? [y/N] '

			# Check if the user wants to update
			read -r -N 1 -p "${prompt}" response

			if [[ "${response}" != $'\n' ]]; then printf '\n'; fi

			if [[ ${response} =~ ^[Yy] ]]; then
				update='1'
			fi
		fi
	fi

	# The user wants to update, do that
	if [[ "${update}" = 1 ]]; then
		printf '\033[1;34m::\033[0m\033[1m Installing firmware updates...\033[0m\n'
		sudo fwupdmgr update
	fi

	# Update Docker containers
	if [[ -f '/etc/systemd/system/docker-compose.service' ]]; then
		printf '\n\033[1;34m::\033[0m\033[1m Reloading Docker Compose service...\033[0m\n'
		reload_start="$(date +'%F %H:%M:%S')"
		sudo systemctl reload docker-compose.service

		# Show service output during the reload
		main_process="$(sudo systemctl status --lines 0 docker-compose.service | sed --quiet --regexp-extended --expression 's/\s*Main PID:\s*([[:digit:]]+)\s*\(docker-compose\).*/\1/p')"
		journalctl --since "${reload_start}" --unit docker-compose.service --quiet --no-pager --output short-unix --no-hostname | cut --delimiter ' ' --fields 2- | grep --invert-match --extended-regexp "Reload(ing|ed) docker-compose.service|\[${main_process}\]:"
	fi

	# Resolve .pacnew files
	printf '\n\033[1;34m::\033[0m\033[1m Checking for pacnew files...\033[0m\n'
	sudo DIFFPROG=colordiff pacdiff

;;
'clear-cache')
	# Delete old versions of packages from the pacman cache
	old="$(sudo paccache --remove --keep 2 2> '/dev/null')"

	# Delete uninstalled packages from the pacman cache
	uninstalled="$(sudo paccache --remove --uninstalled --keep 0 2> '/dev/null')"

	# Combine the package counts and sizes
	total_count="$(( "$(get_paccache_package_count "${old}")" + "$(get_paccache_package_count "${uninstalled}")" ))"
	total_size="$(( "$(get_paccache_package_size "${old}")" + "$(get_paccache_package_size "${uninstalled}")" ))"

	# If we deleted anything from the pacman cache, show that to the user
	if [[ "${total_count}" -ne 0 ]] || [[ "${total_size}" -ne 0 ]]; then
		total_size_human_readable="$(echo "${total_size}" | numfmt --to iec-i --suffix 'B' | sed --regexp-extended 's/([0-9.]+)/\1 /' )"
		echo "${total_count} packages removed (disk space saved: ${total_size_human_readable})"
	fi
;;
*)
	echo 'USAGE
  dpm COMMAND [VALUE]
COMMANDS
  install [PACKAGE ...]
  remove [PACKAGE ...]
  update
  clear-cache'
;;
esac
