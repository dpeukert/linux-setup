#!/bin/bash

cpu_vendor="$(lscpu | sed --quiet '/^Vendor ID/s/[[:space:]]*Vendor ID:[[:space:]]*//p')"

if [ "$cpu_vendor" = 'AuthenticAMD' ]; then
	exit 0
else
	exit 1
fi
