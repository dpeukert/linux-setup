#!/bin/bash

log 'CLEANING UP AFTER INSTALLING PACKAGES'
	if [ -n "$(pacman -Qtdq)" ]; then
		uninstallpackages "$(pacman -Qtdq)"
	fi

	sudo rm -rf "/home/$device/go/"
checkmark

log 'COPYING AND MODIFYING FILES'
	# Scripts
	sudo chmod +x '/usr/local/bin/'*

	# SSH
	chmod 700 "/home/$device/.ssh/"
	chmod 644 "/home/$device/.ssh/authorized_keys" "/home/$device/.ssh/config"

	# hosts file
	if [ -f "/home/$device/isidore_files/$device/hosts" ]; then
		cat "/home/$device/isidore_files/$device/hosts" | sudo tee -a '/etc/hosts' > '/dev/null'
	fi

	# Docker
	if [ -f "/home/$device/private/isidore_files/$device/docker-config" ]; then
		mkdir -p "/home/$device/.docker/"
		chmod 700 "/home/$device/.docker/"

		cat "/home/$device/private/isidore_files/$device/docker-config" > "/home/$device/.docker/config.json"
		chmod 600 "/home/$device/.docker/config.json"
	fi

	# gcac
	mkdir -p "/home/$device/.config/gcac/"
	chmod 700 "/home/$device/.config/gcac/"

	cat "/home/$device/private/isidore_files/archlinux-base/gcac/api_keys" > "/home/$device/.config/gcac/api_keys"
	chmod 600 "/home/$device/.config/gcac/api_keys"

	touch "/home/$device/.config/gcac/providers"
	chmod 600 "/home/$device/.config/gcac/providers"

	# Unused bash config files
	rm -f "$HOME/"{.bashrc,.bash_profile,.bash_logout,.profile,.bashrc.aliases}
checkmark

log 'CONFIGURING GIT'
	git config --global user.name 'Daniel Peukert'
	git config --global user.email 'daniel@peukert.cc'
	git config --global push.autosetupremote true
	git config --global init.defaultBranch 'main'
checkmark

log 'CONFIGURING SERVICES'
	sudo systemctl daemon-reload
	sudo systemctl enable --now 'docker.socket'
	sudo systemctl enable --now 'fstrim.timer'
	sudo systemctl enable --now 'haveged.service'
	sudo systemctl enable --now 'linux-modules-cleanup.service'
	sudo systemctl enable --now 'pkgfile-update.timer'
	sudo systemctl enable --now 'reflector.timer'
	sudo systemctl enable --now 'sshd.service'
	sudo systemctl enable --now 'systemd-timesyncd.service'
checkmark

log 'ADDING GROUPS'
	sudo usermod -aG docker "$device"
checkmark
